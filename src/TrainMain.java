
public class TrainMain {

	
	public static void main(String[] args) {
		
		try {
			NBTrain nbt = new NBTrain();
			nbt.main(null);
			NBPredict nbp = new NBPredict();
			nbp.main(null);
			TitanicEvaluation te = new TitanicEvaluation();
			te.main(null);
		}catch(Exception e) {
			System.out.print(e.toString());
		}
	}
}
